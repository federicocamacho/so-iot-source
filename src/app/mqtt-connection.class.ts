import {Paho} from 'ng2-mqtt/mqttws31';

export class MqttConnection {
  public client: any;

  constructor() {
    this.client = new Paho.MQTT.Client('test.mosquitto.org', 8080, 'UIS/CENTIC');
    this.onConnectionLost();
    this.client.connect({onSuccess: this.onConnected.bind(this)});
  }

  onConnected() {
    console.log('Connected');
    this.client.subscribe('UIS/CENTIC');
  }

  onMessage() {
    this.client.onMessageArrived = (message: Paho.MQTT.Message) => {
      console.log('Message arrived : ' + message.payloadString);
    };
  }

  onConnectionLost() {
    this.client.onConnectionLost = (responseObject: Object) => {
      console.log('Connection lost : ' + JSON.stringify(responseObject));
    };
  }
}
