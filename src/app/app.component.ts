import { Component, ViewChild } from '@angular/core';
import { MqttConnection } from './mqtt-connection.class';
import { Paho } from 'ng2-mqtt/mqttws31';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public lineChartData: any[];
  public lineChartLabels: any[];
  public lineChartType: string;
  public lineChartOptions: any;
  public mqtt: MqttConnection;
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;

  constructor() {
    this.lineChartData = [];
    this.lineChartLabels = [];
    this.lineChartType  = 'line';
    this.lineChartOptions = {};
    this.mqtt = new MqttConnection();
    this.onMessage();
  }

  public randomizeType(): void {
    this.lineChartType = this.lineChartType === 'line' ? 'bar' : 'line';
  }

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  public onMessage(): void {
    this.mqtt.client.onMessageArrived = (message: Paho.MQTT.Message) => {
      console.log('Message arrived : ' + message.payloadString);
      this.lineChartData.push(Number(message.payloadString));
      const date = new Date();
      this.lineChartLabels.push(`${ String(date.getHours()) }:${ String(date.getMinutes()) }:${ String(date.getSeconds()) }`);
      this.chart.chart.update();
    };
  }
}
